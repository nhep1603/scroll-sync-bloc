import 'package:scroll_sync_bloc/model/category.dart';
import 'package:scroll_sync_bloc/model/product.dart';

class ItemCategory {
  const ItemCategory({
    this.category,
    this.product,
  });

  final Category? category;
  final Product? product;
  
  bool get isCategory => category != null;
}