import 'package:scroll_sync_bloc/model/product.dart';

class Category {
  const Category({
    required this.name,
    required this.products,
  });

  final String name;
  final List<Product> products;
}

const categories = [
  Category(
    name: 'One',
    products: [
      Product(
          name: 'Banana',
          description:
              'My family and I like Bananas so much, because they are cheap, delicious and colorful! My family and I like Bananas so much, because they are cheap, delicious and colorful!',
          price: 26.50,
          image: 'assets/images/banana.jpg'),
      Product(
          name: 'Blackberry',
          description:
              'My family and I like Blackberries so much, because they are cheap, delicious and colorful!',
          price: 12.50,
          image: 'assets/images/blackberry.jpg'),
    ],
  ),
  Category(
    name: 'Two',
    products: [
      Product(
          name: 'Grapes',
          description:
              'My family and I like Grapes so much, because they are cheap, delicious and colorful!',
          price: 19.50,
          image: 'assets/images/grapes.jpg'),
      Product(
          name: 'Kiwi',
          description:
              'My family and I like Kiwis so much, because they are cheap, delicious and colorful!',
          price: 16.90,
          image: 'assets/images/kiwi.jpg'),
    ],
  ),
  Category(
    name: 'Three',
    products: [
      Product(
          name: 'Mango',
          description:
              'My family and I like Mangos so much, because they are cheap, delicious and colorful!',
          price: 27.50,
          image: 'assets/images/mange.jpg'),
      Product(
          name: 'Orange',
          description:
              'My family and I like Oranges so much, because they are cheap, delicious and colorful!',
          price: 28.10,
          image: 'assets/images/orange.jpg'),
    ],
  ),
  Category(
    name: 'Four',
    products: [
      Product(
          name: 'Strawberry',
          description:
              'My family and I like Strawberries so much, because they are cheap, delicious and colorful!',
          price: 28.40,
          image: 'assets/images/strawberry.jpg'),
      Product(
          name: 'Watermelon',
          description:
              'My family and I like Watermelons so much, because they are cheap, delicious and colorful!',
          price: 21.30,
          image: 'assets/images/watermelon.jpg'),
    ],
  ),
  Category(
    name: 'Five',
    products: [
      Product(
          name: 'Blueberry',
          description:
              'My family and I like Blueberries so much, because they are cheap, delicious and colorful!',
          price: 19.20,
          image: 'assets/images/blueberry.jpg'),
      Product(
          name: 'Durian',
          description:
              'My family and I like Durians so much, because they are cheap, delicious and colorful!',
          price: 28.50,
          image: 'assets/images/durian.jpg'),
      Product(
          name: 'Blueberry',
          description:
              'My family and I like Blueberries so much, because they are cheap, delicious and colorful!',
          price: 19.20,
          image: 'assets/images/blueberry.jpg'),
      Product(
          name: 'Durian',
          description:
              'My family and I like Durians so much, because they are cheap, delicious and colorful!',
          price: 28.50,
          image: 'assets/images/durian.jpg'),
    ],
  ),
  Category(
    name: 'Six',
    products: [
      Product(
          name: 'Lime',
          description:
              'My family and I like Limes so much, because they are cheap, delicious and colorful!',
          price: 30.40,
          image: 'assets/images/lime.jpg'),
      Product(
          name: 'Lychee',
          description:
              'My family and I like Lychees so much, because they are cheap, delicious and colorful!',
          price: 29.00,
          image: 'assets/images/lychee.jpg'),
    ],
  ),
  Category(
    name: 'Seven',
    products: [
      Product(
          name: 'Papaya',
          description:
              'My family and I like Papayas so much, because they are cheap, delicious and colorful!',
          price: 14.70,
          image: 'assets/images/papaya.jpg'),
      Product(
          name: 'Plum',
          description:
              'My family and I like Plums so much, because they are cheap, delicious and colorful!',
          price: 18.60,
          image: 'assets/images/plum.jpg'),
      Product(
          name: 'Papaya',
          description:
              'My family and I like Papayas so much, because they are cheap, delicious and colorful!',
          price: 14.70,
          image: 'assets/images/papaya.jpg'),
      Product(
          name: 'Plum',
          description:
              'My family and I like Plums so much, because they are cheap, delicious and colorful!',
          price: 18.60,
          image: 'assets/images/plum.jpg'),
      Product(
          name: 'Papaya',
          description:
              'My family and I like Papayas so much, because they are cheap, delicious and colorful!',
          price: 14.70,
          image: 'assets/images/papaya.jpg'),
      Product(
          name: 'Plum',
          description:
              'My family and I like Plums so much, because they are cheap, delicious and colorful!',
          price: 18.60,
          image: 'assets/images/plum.jpg'),
    ],
  ),
];
