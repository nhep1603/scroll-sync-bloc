import 'package:scroll_sync_bloc/model/category.dart';

class TabCategory {
  const TabCategory({
    required this.category,
    required this.select,
    required this.offsetFrom,
    required this.offsetTo,
  });

  final Category category;
  final bool select;
  //? vị trí bắt đầu của tabCategory => vd phần tử 0 : (0,0)
  final double offsetFrom;
  //? vị trí kết thúc của tabCategory => vd phần tử 0 : (0,chiều cao sp*số lượng sản phẩm + chiều cao tên category)
  final double offsetTo;
  TabCategory copyWith({
    Category? category,
    bool? select,
    double? offsetFrom,
    double? offsetTo,
  }) {
    return TabCategory(
      category: category ?? this.category,
      select: select ?? this.select,
      offsetFrom: offsetFrom ?? this.offsetFrom,
      offsetTo: offsetTo ?? this.offsetTo,
    );
  }
}
