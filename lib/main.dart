import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scroll_sync_bloc/app_bloc_observer.dart';
import 'package:scroll_sync_bloc/bloc/scroll_sync_bloc.dart';

import 'model/category.dart';
import 'model/item_category.dart';
import 'model/tab_category.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = AppBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (context) => ScrollSyncBloc(
          categories: categories,
        )..add(ScrollSyncLoaded()),
        child: Home(),
      ),
    );
  }
}

const _backgroundColor = Color(0xFFF6F9FA);
const _blueColor = Color(0xFF0D1862);
const _greenColor = Color(0xFF2BBEBA);

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    
    context.read<ScrollSyncBloc>().add(ScrollSyncInitedTicker(ticker: this));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _backgroundColor,
      body: BlocBuilder<ScrollSyncBloc, ScrollSyncState>(
        builder: (context, state) {
          return state is ScrollSyncLoadSuccess ? Column(
            children: [
              Container(
                color: Colors.white,
                height: 80.0,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'Homepage',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: _blueColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      CircleAvatar(
                        backgroundColor: _greenColor,
                        child: ClipOval(
                          child: Image.asset(
                            'assets/images/banana.jpg',
                            fit: BoxFit.contain,
                            height: 30.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 60.0,
                child: TabBar(
                          controller: context.select(
                              (ScrollSyncBloc bloc) => bloc.tabController),
                          isScrollable: true,
                          indicatorWeight: 0.1,
                          onTap: (index) => context.read<ScrollSyncBloc>().add(ScrollSyncSelected(index: index)),
                          tabs: state.tabs
                              .map((tab) => ItemTab(tab: tab))
                              .toList(),
                        ),
              ),
              Expanded(
                        child: ListView.builder(
                          controller: context.select((ScrollSyncBloc bloc) =>
                              bloc.scrollController),
                          itemCount: state.items.length,
                          padding: EdgeInsets.symmetric(
                            vertical: 10.0,
                            horizontal: 20.0,
                          ),
                          itemBuilder: (context, index) {
                            final item = state.items[index];
                            return state.items[index].isCategory
                                ? ItemIsCategory(
                                    item: item,
                                  )
                                : ItemIsProduct(
                                    item: item,
                                  );
                          },
                        ),
                      )
            ],
          ) : Text('Don\'t have data.');
        },
      ),
    );
  }
}

class ItemTab extends StatelessWidget {
  const ItemTab({
    Key? key,
    required this.tab,
  }) : super(key: key);

  final TabCategory tab;

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: tab.select ? 1 : 0.5,
      child: Card(
        elevation: tab.select ? 6 : 0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            tab.category.name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: _blueColor,
            ),
          ),
        ),
      ),
    );
  }
}

class ItemIsCategory extends StatelessWidget {
  const ItemIsCategory({
    Key? key,
    required this.item,
  }) : super(key: key);
  final ItemCategory item;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      height: categoryHeight,
      child: Card(
        child: Text(
          item.category!.name,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }
}

class ItemIsProduct extends StatelessWidget {
  const ItemIsProduct({
    Key? key,
    required this.item,
  }) : super(key: key);
  final ItemCategory item;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: productHeight,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10.0,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                item.product!.image,
                fit: BoxFit.contain,
              ),
              SizedBox(
                width: 20.0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      item.product!.name,
                      style: TextStyle(
                        color: _blueColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                    Text(
                      item.product!.description,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 12.0,
                      ),
                    ),
                    Text(
                      '\$${item.product!.price.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: _greenColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
