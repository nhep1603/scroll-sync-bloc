part of 'scroll_sync_bloc.dart';

abstract class ScrollSyncEvent extends Equatable {
  const ScrollSyncEvent();

  @override
  List<Object> get props => [];
}

class ScrollSyncLoaded extends ScrollSyncEvent {}

class ScrollSyncInitedTicker extends ScrollSyncEvent {
  const ScrollSyncInitedTicker({
    required this.ticker,
  });
  final TickerProvider ticker;

  @override
  List<Object> get props => [ticker];
}

class ScrollSyncSelected extends ScrollSyncEvent {
  const ScrollSyncSelected({
    required this.index,
  });
  final int index;
  @override
  List<Object> get props => [index];
}

