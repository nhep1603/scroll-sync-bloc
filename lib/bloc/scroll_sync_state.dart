part of 'scroll_sync_bloc.dart';

abstract class ScrollSyncState extends Equatable {
  const ScrollSyncState({
    this.items = const [],
    this.tabs = const [],
  });

  final List<TabCategory>? tabs;
  final List<ItemCategory>? items;

  @override
  List<Object> get props => [];
}

class ScrollSyncInitial extends ScrollSyncState {}

class ScrollSyncLoadSuccess extends ScrollSyncState {
  const ScrollSyncLoadSuccess({
    required this.tabs,
    required this.items,
  });

  final List<TabCategory> tabs;
  final List<ItemCategory> items;

  @override
  List<Object> get props => [tabs, items];
}

class ScrollSyncLoadFailure extends ScrollSyncState {}
