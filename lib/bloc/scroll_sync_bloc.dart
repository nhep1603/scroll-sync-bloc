import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:scroll_sync_bloc/model/category.dart';
import 'package:scroll_sync_bloc/model/item_category.dart';
import 'package:scroll_sync_bloc/model/tab_category.dart';

part 'scroll_sync_event.dart';
part 'scroll_sync_state.dart';

const categoryHeight = 55.0;
const productHeight = 110.0;

class ScrollSyncBloc extends Bloc<ScrollSyncEvent, ScrollSyncState> {
  ScrollSyncBloc({required this.categories}) : super(ScrollSyncInitial()) {
    on<ScrollSyncLoaded>(_onLoad);
    on<ScrollSyncInitedTicker>(_onInitTicker);
    on<ScrollSyncSelected>(_onSelect);
  }

  final List<Category> categories;
  late TabController tabController;
  final ScrollController scrollController = ScrollController();

  _onLoad(ScrollSyncLoaded event, Emitter<ScrollSyncState> emit) {
    double offsetFrom = 0.0;
    double offsetTo = 0.0;
    List<TabCategory> tabs = [];
    List<ItemCategory> items = [];
    if (categories.length != 0) {
      for (var i = 0; i < categories.length; i++) {
        final category = categories[i];
        if (i > 0) {
          offsetFrom += categories[i - 1].products.length * productHeight +
              categoryHeight;
        }
        if (i < categories.length) {
          offsetTo +=
              categories[i].products.length * productHeight + categoryHeight;
        } else {
          offsetTo = double.infinity;
        }
        tabs.add(
          TabCategory(
            category: category,
            select: (i == 0),
            offsetFrom: offsetFrom,
            offsetTo: offsetTo,
          ),
        );
        items.add(ItemCategory(category: category));
        for (var item in category.products) {
          items.add(ItemCategory(product: item));
        }
      }
      emit(ScrollSyncLoadSuccess(
        tabs: tabs,
        items: items,
      ));

      scrollController.addListener(onScrollListener);
    } else {
      emit(ScrollSyncLoadFailure());
    }
  }

  onScrollListener() {
    if (state is ScrollSyncLoadSuccess) {
      for (var i = 0; i < state.tabs!.length; i++) {
        final tab = state.tabs![i];
        List<TabCategory> list = [];
        if (scrollController.offset >= tab.offsetFrom &&
            scrollController.offset <= tab.offsetTo &&
            !tab.select) {
          for (var z = 0; z < state.tabs!.length; z++) {
            final condition = state.tabs![z].category.name == tab.category.name;
            final item = state.tabs![z].copyWith(select: condition);
            list.add(item);
          }
          tabController.animateTo(
            i,
            duration: const Duration(milliseconds: 200),
            curve: Curves.easeInOutQuad,
          );
          // ignore: invalid_use_of_visible_for_testing_member
          emit(ScrollSyncLoadSuccess(
            tabs: list,
            items: state.items!,
          ));
        }
      }
    }
  }

  _onInitTicker(ScrollSyncInitedTicker event, Emitter<ScrollSyncState> emit) {
    tabController = TabController(
      length: categories.length,
      vsync: event.ticker,
    );
  }

  _onSelect(ScrollSyncSelected event, Emitter<ScrollSyncState> emit) {
    if (state is ScrollSyncLoadSuccess) {
      final select = state.tabs![event.index];
      List<TabCategory> list = [];
      for (var i = 0; i < state.tabs!.length; i++) {
        final condition = state.tabs![i].category.name == select.category.name;
        final tab = state.tabs![i].copyWith(select: condition);
        list.add(tab);
      }
      scrollController.animateTo(
        select.offsetFrom,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOutQuad,
      );
      emit(ScrollSyncLoadSuccess(
        tabs: list,
        items: state.items!,
      ));
    }
  }

  @override
  Future<void> close() {
    tabController.dispose();
    scrollController.dispose();
    return super.close();
  }
}
