# scroll_sync_bloc

- Bloc + ObserverBloc
- CircleAvatar() + ClipOval()
- TabBar() + ListView.builder()
- TabController + ScrollController + with SingleTickerProviderStateMixin
- Card() + RoundedRecTangleBorder()

![scroll_sync_bloc](scroll_sync.mp4)
